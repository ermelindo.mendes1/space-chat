from deep_translator import GoogleTranslator

from utils.error_handler import LangException, handle_error


def translate(text, source_lang, target_lang):
    try:
        translator = GoogleTranslator()
        is_supported = translator.is_language_supported(source_lang)
        if not is_supported:
            raise LangException(source_lang)
        return GoogleTranslator(source=source_lang, target=target_lang).translate(text)
    except Exception as e:
        return handle_error(e)
