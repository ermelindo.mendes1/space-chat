// i18n.js

import i18n from "i18next";
import { initReactI18next } from "react-i18next";

const resources = {
  en: {
    translation: {
      hello: "Hello, how can I help you?",
      changeLanguage: "You can change the language by clicking on the flag",
      rockets: "Launch time of the next rocket?",
      tesla: "Tesla's position in space?",
      starlink: "Starlink orbit?",
      iss: "The real-time position of the ISS?",
      language_changed: "The language has been changed into : ",
      error_server: "An error has occurred, please try again later.",
      placeholder_input: "Type your message here...",
      text_button: "Send",
    },
  },
  fr: {
    translation: {
      hello: "Bonjour, comment puis-je vous aider ?",
      changeLanguage:
        "Vous pouvez changer la langue en cliquant sur le drapeau",
      rockets: "Heure de lancement de la prochaine fusée ?",
      tesla: "Position de Tesla dans l'espace ?",
      starlink: "Orbite Starlink ?",
      iss: "Position en temps réel de la Station spatiale internationale ?",
      language_changed: "La langue a été changée en : ",
      error_server: "Une erreur est survenue, veuillez réessayer plus tard.",
      placeholder_input: "Tapez votre message ici...",
      text_button: "Envoyer",
    },
  },
};

i18n.use(initReactI18next).init({
  resources,
  lng: navigator.language.split("-")[0] !== "fr" && navigator.language.split("-")[0] !== "en" ? "en" : navigator.language.split("-")[0],
  interpolation: {
    escapeValue: false,
  },
});

export default i18n;
