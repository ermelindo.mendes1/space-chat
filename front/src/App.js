// App.js
import React from "react";
import Chatbot from "./Chatbot/Chatbot";
import "./App.css";

const App = () => {

  return (
    <div>
      {/* Votre contenu principal */}      
      {/* Chatbot */}
      <Chatbot />
    </div>
  );
};

export default App;